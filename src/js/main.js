(function ($, undefined) {
    $(document).ready(function () {
        
        
        document.getElementById('btn-start').onclick = function() {
            document.getElementById('game-box').classList.add('open');
            document.getElementById('btn-start').classList.add('hidden');
        }


        //DECLARATION VARIABLE
         let game = document.querySelector('.game'),
            result = document.querySelector('.result'),
            btn = document.querySelector('.btn-new-game'),
            fields = document.querySelectorAll('.cell'),
            step = false,
            count = 0;
            svg = document.getElementsByTagName('svg'),
            circle = `<svg class="circle">
                <circle r="45" cx="58" cy="58" stroke="lightseagreen" stroke-width="5" fill="none" stroke-linecap="round"/>
            </svg>
            `,
            cross = `<svg class="cross">
                <line class="first-line" x1="15" y="15" x2="100" y2="100" stroke="lightcoral" stroke-width="5" stroke-linecap="round" />
                <line class="second-line" x1="100" y="15" x2="15" y2="100" stroke="lightcoral" stroke-width="5" stroke-linecap="round" />
            </svg>
            `;

            function stepCross(target) {
                if(target.tagName == 'svg' || target.tagName == 'line' || target.tagName == 'circle') {
                    return;
                }
                target.innerHTML = cross;
                target.classList.add('x');
                count++;
                step = true;
            }

            function stepCircle(target) {
                if(target.tagName == 'svg' || target.tagName == 'line' || target.tagName == 'circle') {
                    return;
                }
                target.innerHTML = circle;  
                target.classList.add('o');
                count++;
                step = false;
            }


            function init(e) {
                if(!step) stepCross(e.target);
                else stepCircle(e.target);
                //step = !step;
                winner();
            }

            function newGame() {
                step = false;
                count = 0;
                result.innerText = '';
                fields.forEach(item => {
                    item.innerHTML = '';
                    item.classList.remove('x', 'o', 'active');
                });
                document.getElementById('custom-modal').classList.remove('open');
                game.addEventListener('click', init);
            }

            function winner() {
                let combination = [
                    [0,1,2],
                    [3,4,5],
                    [6,7,8],
                    [0,3,6],
                    [1,4,7],
                    [2,5,8],
                    [0,4,8],
                    [2,4,6]
                ];

                for(let i = 0; i < combination.length; i++) {
                    
                    if (fields[combination[i][0]].classList.contains('x')  &&
                    fields[combination[i][1]].classList.contains('x') &&
                    fields[combination[i][2]].classList.contains('x')) {
                        setTimeout(() => {
                            fields[combination[i][0]].classList.add('active');
                            fields[combination[i][1]].classList.add('active');
                            fields[combination[i][2]].classList.add('active');
                            document.getElementById('custom-modal').classList.add('open');
                            result.innerText = 'Выиграли Крестики';
                        } , 1500);

                        game.removeEventListener('click', init);
                    }

                    else if (fields[combination[i][0]].classList.contains('o')  &&
                    fields[combination[i][1]].classList.contains('o') &&
                    fields[combination[i][2]].classList.contains('o')) {
                        setTimeout(() => {
                            fields[combination[i][0]].classList.add('active');
                            fields[combination[i][1]].classList.add('active');
                            fields[combination[i][2]].classList.add('active');
                            document.getElementById('custom-modal').classList.add('open');
                            result.innerText = 'Выиграли Нолики';
                        } , 1500);

                        game.removeEventListener('click', init);
                    }

                    else if (count == 9 ) {
                        document.getElementById('custom-modal').classList.add('open');
                        result.innerText = 'Ничья';

                        game.removeEventListener('click', init);
                    }
                }
            }
            
            btn.addEventListener('click', newGame);
            game.addEventListener('click', init);
    });
})(jQuery);
